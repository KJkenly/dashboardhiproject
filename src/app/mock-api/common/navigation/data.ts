/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
 

];
export const compactNavigation: FuseNavigationItem[] = [
    // {
    //     id   : 'pward',
    //     title: 'Productivity IPD',
    //     type : 'basic',
    //     icon : 'mat_outline:group_add',
    //     link : '/pward'
    // },
    // {
    //     id   : 'plr',
    //     title: 'Productivity LR',
    //     type : 'basic',
    //     icon : 'mat_outline:escalator_warning',
    //     link : '/plr'
    // },
    // {
    //     id   : 'per',
    //     title: 'Productivity ER',
    //     type : 'basic',
    //     icon : 'mat_outline:car_repair',
    //     link : '/per'
    // }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    // {
    //     id   : 'pward',
    //     title: 'Productivity IPD',
    //     type : 'basic',
    //     icon : 'mat_outline:group_add',
    //     link : '/pward'
    // },
    // {
    //     id   : 'plr',
    //     title: 'Productivity LR',
    //     type : 'basic',
    //     icon : 'mat_outline:escalator_warning',
    //     link : '/plr'
    // },
    // {
    //     id   : 'per',
    //     title: 'Productivity ER',
    //     type : 'basic',
    //     icon : 'mat_outline:car_repair',
    //     link : '/per'
    // }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'hidashboard',
        title: 'HI DashBoard',
        type : 'aside',
        icon : 'mat_outline:local_hospital',
        children : [
            {
                id   : 'opdashboard',
                title: 'OP DashBoard',
                type : 'basic',
                icon : 'mat_outline:directions_walk',
                link : '/opdashboard'
            },
            {
                id   : 'ipdashboard',
                title: 'IP DashBoard',
                type : 'basic',
                icon : 'mat_outline:airline_seat_individual_suite',
                link : '/ipdashboard'
            }
        ]
    },
    // {
    //     id   : 'opdashboard',
    //     title: 'OP DashBoard',
    //     type : 'basic',
    //     icon : 'mat_outline:accessible_forward',
    //     link : '/opdashboard'
    // },
    // {
    //     id   : 'ipdashboard',
    //     title: 'IP DashBoard',
    //     type : 'basic',
    //     icon : 'mat_outline:airline_seat_individual_suite',
    //     link : '/ipdashboard'
    // },
    //เมนูรายงานย่อย statement
    {
        id   : 'Rep',
        title: 'บัญชีลูกหนี้',
        type : 'aside',
        icon : 'mat_outline:auto_graph',
        children : [
                {
                    id   : 'Rep-OP',
                    title: 'บัญชีลูกหนี้-OP',
                    type : 'aside',
                    icon : 'mat_outline:menu_open',
                    children:[

                        {
                            id   : 'rep-op-ofc',
                            title: 'OP-OFC',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-op-ofc'
                        },
                        {
                            id   : 'rep-op-stp',
                            title: 'OP-STP',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-op-stp'
                        },
                        {
                            id   : 'rep-op-ucs',
                            title: 'OP-UCS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-op-ucs'
                        },
                        {
                            id   : 'rep-op-bkk',
                            title: 'OP-BKK',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-op-bkk'
                        },
                        {
                            id   : 'rep-ipop-sss',
                            title: 'IPOP-SSS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-ipop-sss'
                        }
                    ]
            },
            {
                    id   : 'REP-IP',
                    title: 'บัญชีลูกหนี้-IP',
                    type : 'aside',
                    icon : 'mat_outline:menu_open',
                    children:[

                        {
                            id   : 'rep-ip-ofc',
                            title: 'IP-OFC',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-ip-ofc'
                        },
                        {
                            id   : 'rep-ip-stp',
                            title: 'IP-STP',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-ip-stp'
                        },
                        {
                            id   : 'rep-ip-ucs',
                            title: 'IP-UCS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-ip-ucs'
                        },
                        {
                            id   : 'rep-ip-bkk',
                            title: 'IP-BKK',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-ip-bkk'
                        },
                        {
                            id   : 'rep-ipop-sss',
                            title: 'IPOP-SSS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/rep-ipop-sss'
                        }
                    ]
            },

        ],

    },
    //เมนูรายงานย่อย statement
    {
        id   : 'statement',
        title: 'Statement',
        type : 'aside',
        icon : 'mat_outline:account_balance_wallet',
        children : [
                {
                    id   : 'statement-OP',
                    title: 'statement-OP',
                    type : 'aside',
                    icon : 'mat_outline:menu_open',
                    children:[

                        {
                            id   : 'stm-op-ofc',
                            title: 'OP-OFC',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-op-ofc'
                        },
                        {
                            id   : 'stm-op-stp',
                            title: 'OP-STP',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-op-stp'
                        },
                        {
                            id   : 'stm-op-ucs',
                            title: 'OP-UCS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-op-ucs'
                        },
                        {
                            id   : 'stm-op-bkk',
                            title: 'OP-BKK',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-op-bkk'
                        },
                        {
                            id   : 'stm-ipop-sss',
                            title: 'IPOP-SSS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-ipop-sss'
                        }
                    ]
            },
            {
                    id   : 'Statement-IP',
                    title: 'Statement-IP',
                    type : 'aside',
                    icon : 'mat_outline:menu_open',
                    children:[

                        {
                            id   : 'stm-ip-ofc',
                            title: 'IP-OFC',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-ip-ofc'
                        },
                        {
                            id   : 'stm-ip-stp',
                            title: 'IP-STP',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-ip-stp'
                        },
                        {
                            id   : 'stm-ip-ucs',
                            title: 'IP-UCS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-ip-ucs'
                        },
                        {
                            id   : 'stm-ip-bkk',
                            title: 'IP-BKK',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-ip-bkk'
                        },
                        {
                            id   : 'stm-ipop-sss',
                            title: 'IPOP-SSS',
                            type : 'basic',
                            icon : 'mat_outline:arrow_right',
                            link : '/stm-ipop-sss'
                        }
                    ]
            },

        ],

    },
    {
        id   : 'aipn',
        title: 'AIPN',
        type : 'basic',
        icon : 'mat_outline:accessibility',
        link : '/aipn'
    },
    {
        id   : 'ssop',
        title: 'SSOP',
        type : 'basic',
        icon : 'mat_outline:person_pin',
        link : '/ssop'
    },
     {
         id   : 'bookclaim',
         title: 'ทะเบียนผู้มารับบริการ',
         type : 'basic',
         icon : 'mat_outline:folder_special',
         link : '/bookclaim'
     }
    // {
    //     id   : 'export16f',
    //     title: '16F(TEST)',
    //     type : 'basic',
    //     icon : 'mat_outline:folder_special',
    //     link : '/export16f'
    // },
];
