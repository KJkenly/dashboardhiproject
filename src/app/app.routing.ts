import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';

// @formatter:off
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    //กำหนดให้เปิดหน้าแรกไปที่หน้า Sing in
    {path: '', pathMatch : 'full', redirectTo: 'sign-in'},

    // Redirect signed-in user to the '/example'
    //
    // After the user signs in, the sign-in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: 'home'},

// mark with no sign in ##
    // Auth routes for guests
    {
        path: '',
        canMatch: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'confirmation-required', loadChildren: () => import('app/modules/auth/confirmation-required/confirmation-required.module').then(m => m.AuthConfirmationRequiredModule)},
            {path: 'forgot-password', loadChildren: () => import('app/modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)},
            {path: 'reset-password', loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)},
            {path: 'sign-in', loadChildren: () => import('app/modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule)},
            {path: 'sign-up', loadChildren: () => import('app/modules/auth/sign-up/sign-up.module').then(m => m.AuthSignUpModule)}
        ]
    },

    // Auth routes for authenticated users
    {
        path: '',
        canMatch: [AuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'sign-out', loadChildren: () => import('app/modules/auth/sign-out/sign-out.module').then(m => m.AuthSignOutModule)},
            {path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule)}
        ]
    },

    // Landing routes
    //canMatch: [AuthGuard], เช็ค Login  AuthGuard , ไม่เช็ค Login NoAuthGuard
    {
        path: '',
        canMatch: [AuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {path: 'home', loadChildren: () => import('app/modules/landing/home/home.module').then(m => m.LandingHomeModule)},
            //STM IP
            {path: 'stm-ip-bkk',loadChildren: () => import('app/modules/landing/stm-ip-bkk/stm-ip-bkk.module').then(m => m.StmIpBkkModule)},
            {path: 'stm-ipop-sss',loadChildren: () => import('app/modules/landing/stm-ipop-sss/stm-ipop-sss.module').then(m => m.StmIpSssModule)},
            {path: 'stm-ip-ofc',loadChildren: () => import('app/modules/landing/stm-ip-ofc/stm-ip-ofc.module').then(m => m.StmIpOfcModule)},
            {path: 'stm-ip-stp',loadChildren: () => import('app/modules/landing/stm-ip-stp/stm-ip-stp.module').then(m => m.StmIpStpModule)},
            {path: 'stm-ip-ucs',loadChildren: () => import('app/modules/landing/stm-ip-ucs/stm-ip-ucs.module').then(m => m.StmIpUcsModule)},
            {path: 'stm-ip-bkk',loadChildren: () => import('app/modules/landing/stm-ip-bkk/stm-ip-bkk.module').then(m => m.StmIpBkkModule)},
            //STM OP
            {path: 'stm-op-bkk',loadChildren: () => import('app/modules/landing/stm-op-bkk/stm-op-bkk.module').then(m => m.StmOpBkkModule)},
            {path: 'stm-op-lgo',loadChildren: () => import('app/modules/landing/stm-op-lgo/stm-op-lgo.module').then(m => m.StmOpLgoModule)},
            {path: 'stm-op-ofc',loadChildren: () => import('app/modules/landing/stm-op-ofc/stm-op-ofc.module').then(m => m.StmOpOfcModule)},
            {path: 'stm-op-stp',loadChildren: () => import('app/modules/landing/stm-op-stp/stm-op-stp.module').then(m => m.StmOpStpModule)},
            {path: 'stm-op-ucs',loadChildren: () => import('app/modules/landing/stm-op-ucs/stm-op-ucs.module').then(m => m.StmOpUcsModule)},
            //Rep IP
            {path: 'rep-ipop-sss',loadChildren: () => import('app/modules/landing/rep-ipop-sss/rep-ipop-sss.module').then(m => m.RepIpopSssModule)},
            {path: 'rep-ip-ofc',loadChildren: () => import('app/modules/landing/rep-ip-ofc/rep-ip-ofc.module').then(m => m.RepIpOfcModule)},
            {path: 'rep-ip-lgo',loadChildren: () => import('app/modules/landing/rep-ip-lgo/rep-ip-lgo.module').then(m => m.RepIpLgoModule)},
            {path: 'rep-ip-ucs',loadChildren: () => import('app/modules/landing/rep-ip-ucs/rep-ip-ucs.module').then(m => m.RepIpUcsModule)},
            {path: 'rep-ip-bkk',loadChildren: () => import('app/modules/landing/rep-ip-bkk/rep-ip-bkk.module').then(m => m.RepIpBkkModule)},
            {path: 'rep-ip-stp',loadChildren: () => import('app/modules/landing/rep-ip-stp/rep-ip-stp.module').then(m => m.RepIpStpModule)},
            //Rep OP
            {path: 'rep-op-lgo',loadChildren: () => import('app/modules/landing/rep-op-lgo/rep-op-lgo.module').then(m => m.RepOpLgoModule)},
            {path: 'rep-op-ofc',loadChildren: () => import('app/modules/landing/rep-op-ofc/rep-op-ofc.module').then(m => m.RepOpOfcModule)},
            {path: 'rep-op-ucs',loadChildren: () => import('app/modules/landing/rep-op-ucs/rep-op-ucs.module').then(m => m.RepOpUcsModule)},           
            {path: 'rep-op-bkk',loadChildren: () => import('app/modules/landing/rep-op-bkk/rep-op-bkk.module').then(m => m.RepOpBkkModule)},
            {path: 'rep-op-stp',loadChildren: () => import('app/modules/landing/rep-op-stp/rep-op-stp.module').then(m => m.RepOpStpModule)},
            
            //AIPN
            {path: 'aipn',loadChildren: () => import('app/modules/landing/aipn/aipn.module').then(m => m.AipnModule)},
            //SSOP
            {path: 'ssop',loadChildren: () => import('app/modules/landing/ssop/ssop.module').then(m => m.SsopModule)},
            //Refer OPD
            {path: 'op-rfro',loadChildren: () => import('app/modules/landing/op-referout/op-referout.module').then(m => m.OpReferoutModule)},
            {path: 'op-rfri',loadChildren: () => import('app/modules/landing/op-referin/op-referin.module').then(m => m.OpReferinModule)},
            // Refer IPD
            {path: 'ip-rfro',loadChildren: () => import('app/modules/landing/ip-referout/ip-referout.module').then(m => m.IpReferoutModule)},
            {path: 'ip-rfri',loadChildren: () => import('app/modules/landing/ip-referin/ip-referin.module').then(m => m.IpReferinModule)},
 
            {path: 'ioprt',loadChildren: () => import('app/modules/landing/ioprt/ioprt.module').then(m => m.IoprtModule)},
            {path: 'oprt',loadChildren: () => import('app/modules/landing/oprt/oprt.module').then(m => m.OprtModule)},

            //Profile
            {path: 'profile',loadChildren: () =>import('app/modules/landing/profile/profile.module').then(m => m.ProfileModule)},
            
            //Dashboard 
            {path: 'opdashboard',loadChildren:() =>import('app/modules/landing/dashboard/dashboard.module').then(m => m.DashboardModule)},

            //Dashboard 
            {path: 'ipdashboard',loadChildren:() =>import('app/modules/landing/ip-dashboard/ip-dashboard.module').then(m => m.IpDashboardModule)},
            //Export16f
            {path: 'export16f',loadChildren:()=>import('app/modules/landing/export16f/export16f.module').then(m => m.Export16fModule)},
            //BookClaim
            {path: 'bookclaim',loadChildren:()=>import('app/modules/landing/bookclaim/bookclaim.module').then(m => m.BookclaimModule)}
        ]
    },

    // Admin routes
    {
        path: '',
        canMatch: [AuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {path: 'exampledd', loadChildren: () => import('app/modules/admin/example/example.module').then(m => m.ExampleModule)},
        ]
    }
];
