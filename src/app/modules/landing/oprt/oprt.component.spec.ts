import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OprtComponent } from './oprt.component';

describe('OprtComponent', () => {
  let component: OprtComponent;
  let fixture: ComponentFixture<OprtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OprtComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OprtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
