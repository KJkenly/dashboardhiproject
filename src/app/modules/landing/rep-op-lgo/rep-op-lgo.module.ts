import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepOpLgoRoutingModule } from './rep-op-lgo-routing.module';
import { RepOpLgoComponent } from './rep-op-lgo.component';


@NgModule({
  declarations: [
    RepOpLgoComponent
  ],
  imports: [
    CommonModule,
    RepOpLgoRoutingModule
  ]
})
export class RepOpLgoModule { }
