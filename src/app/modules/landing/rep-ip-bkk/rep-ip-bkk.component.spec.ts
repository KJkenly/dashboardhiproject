import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepIpBkkComponent } from './rep-ip-bkk.component';

describe('RepIpBkkComponent', () => {
  let component: RepIpBkkComponent;
  let fixture: ComponentFixture<RepIpBkkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepIpBkkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepIpBkkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
