import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';



import { RepIpBkkRoutingModule } from './rep-ip-bkk-routing.module';
import { RepIpBkkComponent } from './rep-ip-bkk.component';
import { ExcelService } from 'app/services/excel.service';
import { CircularProgressComponent } from '../../../components/circular-progress/circular-progress.component'

@NgModule({
  declarations: [
    RepIpBkkComponent
  ],
  providers:[ExcelService],
  imports: [
    RepIpBkkRoutingModule,
    MatPaginatorModule,
    SharedModule,
    CircularProgressComponent,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepIpBkkModule { }
