import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmOpOfcComponent } from './stm-op-ofc.component';

describe('StmOpOfcComponent', () => {
  let component: StmOpOfcComponent;
  let fixture: ComponentFixture<StmOpOfcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmOpOfcComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmOpOfcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
