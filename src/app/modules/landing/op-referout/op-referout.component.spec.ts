import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpReferoutComponent } from './op-referout.component';

describe('OpReferoutComponent', () => {
  let component: OpReferoutComponent;
  let fixture: ComponentFixture<OpReferoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpReferoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OpReferoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
