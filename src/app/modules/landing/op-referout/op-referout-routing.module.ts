import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpReferoutComponent } from './op-referout.component';

const routes: Routes = [
  {
    path:'',component:OpReferoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpReferoutRoutingModule { }
