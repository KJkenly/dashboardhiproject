import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';
import { ExcelService } from 'app/services/excel.service';

import { IpReferoutRoutingModule } from './ip-referout-routing.module';
import { IpReferoutComponent } from './ip-referout.component';


@NgModule({
  declarations: [
    IpReferoutComponent
  ],
  providers:[ExcelService],
  imports: [
    IpReferoutRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class IpReferoutModule { }
