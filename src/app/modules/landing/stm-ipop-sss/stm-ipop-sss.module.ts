import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmIpSssRoutingModule } from './stm-ipop-sss-routing.module';
import {StmIpSssComponent } from './stm-ipop-sss.component'
import { ExcelService } from 'app/services/excel.service';
@NgModule({
  declarations: [
    StmIpSssComponent
  ],
  providers:[ExcelService],
  imports: [
    StmIpSssRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmIpSssModule { }
