import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmIpSssComponent} from './stm-ipop-sss.component';

const routes: Routes = [
  {
    path: '',component:StmIpSssComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmIpSssRoutingModule { }
