import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmIpSssComponent } from './stm-ipop-sss.component';

describe('StmIpSssComponent', () => {
  let component: StmIpSssComponent;
  let fixture: ComponentFixture<StmIpSssComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmIpSssComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmIpSssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
