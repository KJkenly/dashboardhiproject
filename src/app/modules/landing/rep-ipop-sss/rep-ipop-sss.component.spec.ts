import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepIpopSssComponent } from './rep-ipop-sss.component';

describe('RepIpopSssComponent', () => {
  let component: RepIpopSssComponent;
  let fixture: ComponentFixture<RepIpopSssComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepIpopSssComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepIpopSssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
