import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/bookclaim/bookclaim.model'

import { BookclaimService } from '../../../services/bookclaim.service'


export interface _bookclaim {
  vn:any;
  hn:any;
  vstdttm:any;
  vsttime:any;
  sex:any;
  fullname:any;
  namepttype:any;
  inscl:any;
  insure:any;
  checkinscl:any;
  checkby:any;
  hospmain:any;
  hospsub:any;
  namecln:any;
  nameregis:any;
  claimcode:any;
  queue_number:any;
  priority_name:any;
  pop_id:any;
  claim_status:any;
  screen_status:any;
  ovstost:any;
  an:any;
  icon:any;
  hours_difference:any;
  claimtype:any;
  age:any;
  authen_code_closevisit:any;
}


@Component({
  selector: 'app-bookclaim',
  templateUrl: './bookclaim.component.html',
  styleUrls: ['./bookclaim.component.scss']
})
export class BookclaimComponent {

//item Aipn statusflg A
itemBookclaim : any =[];
itemsExeclBookclaim:any = []; 

//get Value Count status
register_all: any=0;
screen_null: any=0;
screen_notnull: any=0;
bookclaim_null: any=0;
bookclaim_notnull: any=0;
claimcode_null: any=0;
claimcode_notnull: any=0;


startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
bookclaimdisplayedColumns: string[] = ['age','icon','vn','an','hn','vstdttm','vsttime','fullname','sex',
  'namecln','namepttype','inscl','hospmain','hospsub','claimtype','claimcode','authen_code_closevisit','claim_status','screen_status'
  
];

@ViewChild(MatPaginator) paginator: MatPaginator;
ngAfterViewInit_bookclaim() {
    this.itemBookclaim.paginator = this.paginator;
  }

  constructor(
    private router: Router,
    private bookclaimService:BookclaimService,
    private excelService : ExcelService
) {} 

ngOnInit():void{
  // this.getinfoBookClaim()
}

async getinfoBookClaim(){
let info: any = {
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info',info);
  await this.getDataBookclaim(info);
  await this.getbookclaim_all_Statusflg(info);

}

async getDataBookclaim(info){
    this.itemBookclaim= [];
    this.itemsExeclBookclaim=[];
    console.log('xx');
    
    try {
        console.log('yy');
        
        let rs: any = await this.bookclaimService.getselete_raw_bookclaim(info);
        let data:any = rs.results.rows;
        console.log('A',rs);
        if (rs.results.ok){
          this.itemsExeclBookclaim = data;
          this.itemBookclaim = new MatTableDataSource<_bookclaim>(data);
          this.ngAfterViewInit_bookclaim();  
        }
      } catch (error: any) {
          console.log();
      }
}

async getbookclaim_all_Statusflg(info){
  try{
    let rs:any = await this.bookclaimService.getbookclaim_all_statusflg(info);
    let data:any = rs[0];
    console.log(rs);
    if(rs.length > 0){
      this.register_all =data.total_all;
      this.claimcode_null = data.claimcode_null;
      this.claimcode_notnull = data.claimcode_notnull;
      this.bookclaim_null = data.claimstatus_null;
      this.bookclaim_notnull = data.claimstatus_notnull;
      this.screen_null = data.screen_null;
      this.screen_notnull = data.screen_notnull;
    }    
  }catch(error:any){
    console.log();    
  }
}
exportAsXLSXBookclaim():void {
  this.excelService.exportAsExcelFile(this.itemsExeclBookclaim, 'ExporttoExcel');
}

getAttact_detail(vn){
  console.log('c',vn);
  
}
}
