import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookclaimComponent } from './bookclaim.component';

describe('BookclaimComponent', () => {
  let component: BookclaimComponent;
  let fixture: ComponentFixture<BookclaimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookclaimComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BookclaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
