import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BookclaimComponent } from './bookclaim.component'
const routes: Routes = [
  {
    path:'',component:BookclaimComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookclaimRoutingModule { }
