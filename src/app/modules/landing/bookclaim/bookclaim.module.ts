import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { BookclaimRoutingModule } from './bookclaim-routing.module';
import { BookclaimComponent } from './bookclaim.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    BookclaimComponent
  ],
  providers:[ExcelService],
  imports: [
    BookclaimRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})

export class BookclaimModule { }
