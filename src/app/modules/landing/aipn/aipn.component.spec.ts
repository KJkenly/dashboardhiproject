import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AipnComponent } from './aipn.component';

describe('AipnComponent', () => {
  let component: AipnComponent;
  let fixture: ComponentFixture<AipnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AipnComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AipnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
