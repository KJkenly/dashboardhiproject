import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AipnComponent } from './aipn.component'
const routes: Routes = [
  {
    path:'',component:AipnComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AipnRoutingModule { }
