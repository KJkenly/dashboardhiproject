import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmOpBkkRoutingModule } from './stm-op-bkk-routing.module';
import { StmOpBkkComponent } from './stm-op-bkk.component';
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmOpBkkComponent
  ],
  providers:[ExcelService],
  imports: [
    StmOpBkkRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmOpBkkModule { }
