import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ChartType } from '../../landing/stm-ip-ofc/stm-ip-ofc.model'
import { ExcelService } from 'app/services/excel.service';

import { OpbkkService } from '../../../services/opbkk.service'

export interface _stpopbkk {
  stm_no:any;
  repno: any;
  no: any;
  hn: any;
  an: any;
  cid: any;
  fullname: any;
  date_serv: any;
  date_dch: any;
  projcode: any;
  adjrw: any;
  charge: any;
  insure: any;
  in_room: any;
  in_prcd: any;
  in_meditem: any;
  in_dx: any;
  in_carfare: any;
  in_case: any;
  in_etc: any;
  total_summary: any;
  diff:any;
}

@Component({
  selector: 'app-stm-op-bkk',
  templateUrl: './stm-op-bkk.component.html',
  styleUrls: ['./stm-op-bkk.component.scss']
})
export class StmOpBkkComponent {

  //stm ipbkk
itemstmopbkk : any =[];
itemsExeclStmopbkk:any = [];

all_case: any;
debt: any;
receive: any;
sum_diff: any;
diffgain: any;
diffloss: any;
repno:any;

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
stmopbkkdisplayedColumns: string[] = ['stm_no','repno','no','hn','an',
'cid', 'fullname', 'date_serv','date_dch','projcode','adjrw','charge','insure',
'in_room','in_prcd','in_meditem','in_dx','in_carfare','in_case','in_etc','total_summary','diff'];

@ViewChild(MatPaginator) paginator: MatPaginator;
ngAfterViewInit_stmopbkk() {
    this.itemstmopbkk.paginator = this.paginator;
  }

  constructor(
    private router: Router,
    private opbkkService: OpbkkService,
    private alertService: AlertService,
    private excelService:ExcelService
   
   ){}

   ngOnInit(): void {
    this.getStmopBkk();
  }

  async getStmopBkk(){
    // let info: any = {
    // "repno": this.repno,
    // }
    // console.log(this.repno);

    // await this.stmopbkk(info);
    // await this.stmopbkk_sum(info);
    let info: any = {
      "stm_no": this.repno,
      }
      console.log(this.repno);
  
      await this.stmopbkk_stmno(info);
      await this.stmopbkk_sum_stmno(info);
  }

 async stmopbkk(info){
    this.itemstmopbkk= [];
    this.itemsExeclStmopbkk=[];
    try {
        let rs: any = await this.opbkkService.getstmopbkk(info);
        let data:any = rs.results.rows;
        console.log(rs);
        if (rs.results.ok){
          this.itemsExeclStmopbkk = data;
          this.itemstmopbkk = new MatTableDataSource<_stpopbkk>(data);
          this.ngAfterViewInit_stmopbkk();

        }
      } catch (error: any) {
          console.log();
      }
 }
 async stmopbkk_stmno(info){
  this.itemstmopbkk= [];
  this.itemsExeclStmopbkk=[];
  try {
      let rs: any = await this.opbkkService.getstmopbkk_stmno(info);
      let data:any = rs.results.rows;
      console.log(rs);
      if (rs.results.ok){
        this.itemsExeclStmopbkk = data;
        this.itemstmopbkk = new MatTableDataSource<_stpopbkk>(data);
        this.ngAfterViewInit_stmopbkk();

      }
    } catch (error: any) {
        console.log();
    }
}
 async stmopbkk_sum(info){
    try {
      let rs: any = await this.opbkkService.getstmopbkk_sum(info);
      console.log(rs);
      if (rs.results.ok){
        this.all_case = rs.results.rows[0].all_case;
        console.log('all_case',this.all_case);
        
        this.debt = rs.results.rows[0].debt;
        this.receive = rs.results.rows[0].receive;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
        console.log();
    }

  }
  async stmopbkk_sum_stmno(info){
    try {
      let rs: any = await this.opbkkService.getstmopbkk_sum_stmno(info);
      console.log(rs);
      if (rs.results.ok){
        this.all_case = rs.results.rows[0].all_case;
        console.log('all_case',this.all_case);
        
        this.debt = rs.results.rows[0].debt;
        this.receive = rs.results.rows[0].receive;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
        console.log();
    }

  }
 exportAsXLSXStmopBkk():void {
    this.excelService.exportAsExcelFile(this.itemsExeclStmopbkk, 'ExporttoExcel');
  }

}