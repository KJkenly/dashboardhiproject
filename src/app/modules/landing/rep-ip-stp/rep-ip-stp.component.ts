import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-ip-stp/rep-ip-stp.model'

import { IpstpService } from '../../../services/ipstp.service'

export interface _ipstpnull {
  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2: any;
  rest_debt: any;
  projcode: any;
}

export interface _ipstpnotnull_stm {

  an: any;
  hn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  l_stay: any;
  charge: any;
  paid: any;
  debt: any;
  debt2: any;
  repno: any;
  error_code: any;
  error_name: any;
  remark_data: any;
  total_paid: any;
  rep_diff: any;
  rep_diff2: any;
  rest_debt: any;
  projcode: any;

}

export interface _ipstpnotnull {
  hn: any;
  an: any;
  cid: any;
  fullname: any;
  acc_name: any;
  admitdate: any;
  dchdate: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  adjrw: any;
  total_summary: any;
  projcode: any;
  rep_diff: any;
  rep_diff2: any;
  receipt_no: any;
}

export interface _ipstpaccbydate {
  dchdate: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}
export interface _errorcode {
  error_code: any;
  error_name: any;
  counterrorcode: any;
}

@Component({
  selector: 'app-rep-ip-stp',
  templateUrl: './rep-ip-stp.component.html',
  styleUrls: ['./rep-ip-stp.component.scss']
})
export class RepIpStpComponent {
  //count datatable
  countRowstab1: any;
  countRowstab2: any;
  countRowstab3: any;
  countRowstab4: any;
  countRowstab5: any;
  //สร้างตัวแปร items,datestart,dateend
  //itemerrorcode
  itemerrorcode: any = [];
  itemerrorcodeExecl: any = [];

  //ipstpaccbydate
  itemIpstpaccbydate: any = [];
  itemsExeclIpstpaccbydate: any = [];

  //ipstpnotNull
  itemIpstpnotNull: any = [];
  itemsExeclIpstpnotNull: any = [];

  //ipstpNull
  itemIpstpNull: any = [];
  itemsExeclIpstpNull: any = [];

  //accnull
  ipstpaccnull_count: any = 0;
  ipstpaccnull_sum: any = 0;

  //debt_account
  itemaccount: any = [];
  selecteditemaccount: any = '';

  //รอดำเนินการมี repno แต่ไม่มี stm
  ipstpaccnotnullstm_count: any = 0;
  ipstpaccnotnullstm_sum: any = 0;

  //ipofcnotNull_stm
  itemIpstpnotnull_stm: any = [];
  itemsExeclIpstpNotNull_stm: any = [];

  //accnotnull
  all_notnullcase: any = 0;
  debit_notnull: any = 0;
  recieve: any = 0;
  sum_diff: any = 0;
  diffgain: any = 0;
  diffloss: any = 0;
  ipstpcall: any = 0;
  ipstpsumdebit: any = 0;

  //circular
  percent_null: any = 0;
  percent_rep: any = 0;
  percent_success: any = 0;

  startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  isCheckbox: boolean = false;

  // ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
  ipstpnulldisplayedColumns: string[] = ['hn', 'an', 'cid', 'fullname',
    'acc_name', 'admitdate', 'dchdate', 'charge', 'paid', 'debt', 'repno', 'projcode', 'error_code', 'error_name'];
  ipstpnotnullstmdisplayedColumns: string[] = ['hn', 'an', 'cid', 'fullname',
    'acc_name', 'admitdate', 'dchdate', 'charge', 'paid', 'debt', 'repno', 'projcode', 'error_code', 'error_name'];

  ipstpnotnulldisplayedColumns: string[] = ['hn', 'an', 'cid', 'fullname',
    'acc_name', 'admitdate', 'dchdate', 'charge', 'paid', 'debt', 'repno', 'projcode', 'adjrw',
    'total_summary', 'rep_diff', 'rep_diff2', 'receipt_no'];

  ipstpaccbydatedisplayedColumns: string[] = ['dchdate', 'allcase', 'debit', 'nullcase',
    'nulldebit', 'notnullcase', 'notnulldebit', 'recieve', 'diff'];

  errorcodedisplayedColumns: string[] = ['error_code', 'error_name', 'counterror'];

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('paginator2') paginator2: MatPaginator;
  @ViewChild('paginator3') paginator3: MatPaginator;
  @ViewChild('paginator4') paginator4: MatPaginator;
  @ViewChild('paginator5') paginator5: MatPaginator;

  ngAfterViewInit_ipstpnull() {
    this.itemIpstpNull.paginator = this.paginator;
  }
  ngAfterViewInit_ipstpNotnull_stm() {
    this.itemIpstpnotnull_stm.paginator = this.paginator2;
  }
  ngAfterViewInit_ipstpnotnull() {
    this.itemIpstpnotNull.paginator = this.paginator3;
  }
  ngAfterViewInit_ipstpaccbydate() {
    this.itemIpstpaccbydate.paginator = this.paginator4;
  }
  ngAfterViewInit_ipstperrorcode() {
    this.itemerrorcode.paginator = this.paginator5;
  }

  constructor(
    private router: Router,
    private ipstpService: IpstpService,
    private excelService: ExcelService
  ) { }


  ngOnInit(): void {
    this.isCheckbox = true;
    this.getLoadData()
    //this.getipstpacc()
  }

  //get DATA TABLE
  async getLoadData() {
    await this.getLookup_debtaccount();
  }
  //get DATA Table debt_account of db HICM
  async getLookup_debtaccount() {
    let lcACC: any = await this.ipstpService.getselect_debt_account_hi();
    console.log('lcACC', lcACC);

    this.itemaccount = lcACC;
  }
  async getipstpacc() {

    let lcCode: any = this.selecteditemaccount; //stp
    let info: any = {
      "accType": lcCode,
      "startDate": moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
      "endDate": moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
    }
    console.log('info:', info);

    //ผู้ป่วยส่งเบิกทั้งหมด OK
    await this.ipCounstpAll(info)
    //ดำเนินการเสร็จ OK
    await this.ipstpaccnotnull(info);
    //รอดำเนินการ OK
    await this.ipstpaccnull(info);
    //มี repno แต่ไม่มี statement
    await this.ipstpnotnull_stm(info);

    //tab 1 ok
    await this.tab_ipstpnull(info);
    //tab 2 ok
    await this.tab_ipstpaccnotnull_stm(info);
    //tab 3 ok
    await this.tab_ipstpnotnull(info);
    //tab 4 ok
    await this.tab_ipstpaccbydate(info);
    //tab 5 ok
    await this.tab_ipstpbyerror(info);
    //Sum Total
    await this.ipstpall();
    await this.circularprogress();
  }
  async ipCounstpAll(info) {
    try {
      let rs: any = await this.ipstpService.getstmipCount(info);
      let data: any = rs.results;
      console.log('xx', rs);
      if (rs.results.ok) {
        this.ipstpcall = rs.results.rows[0].all_ip;

      }
    } catch (error: any) {
      console.log(error);
    }
  }

  async ipstpaccnull(info) {
    try {
      let rs: any = await this.ipstpService.getipstpaccnull(info);
      console.log(rs);
      if (rs.results.ok) {
        // this.itemsExecl = rs;
        this.ipstpaccnull_count = rs.results.rows[0].count_an;
        this.ipstpaccnull_sum = rs.results.rows[0].sum_debt;
        console.log(this.ipstpaccnull_count);

      }
    } catch (error: any) {
      console.log();

    }

  }
  async ipstpnotnull_stm(info) {
    try {
      let rs: any = await this.ipstpService.getipstpnotnull_stm(info);
      console.log(rs);
      if (rs.results.ok) {
        // this.itemsExecl = rs;
        this.ipstpaccnotnullstm_count = rs.results.rows[0].count_an;
        this.ipstpaccnotnullstm_sum = rs.results.rows[0].sum_debt;
        console.log(this.ipstpaccnotnullstm_sum);

      }
    } catch (error: any) {
      console.log();

    }

  }
  async ipstpaccnotnull(info) {
    try {
      let rs: any = await this.ipstpService.getipstpaccnotnull(info);
      console.log(rs);
      if (rs.results.ok) {
        // this.itemsExecl = rs;
        this.all_notnullcase = rs.results.rows[0].all_notnullcase;
        this.debit_notnull = rs.results.rows[0].debit_notnull;
        this.recieve = rs.results.rows[0].recieve;
        this.sum_diff = rs.results.rows[0].sum_diff;
        this.diffgain = rs.results.rows[0].diffgain;
        this.diffloss = rs.results.rows[0].diffloss;

      }
    } catch (error: any) {
      console.log();
    }

  }
  async tab_ipstpaccnotnull_stm(info) {
    this.itemIpstpnotnull_stm = [];
    this.itemsExeclIpstpNotNull_stm = [];
    try {
      let rs: any = await this.ipstpService.getipstpaccnotnull_stm(info);
      let data: any = rs.results.rows
      let xx: string = data
      console.log('xxxxx', xx);

      this.countRowstab5 = xx.length

      if (rs.results.ok) {
        this.itemsExeclIpstpNotNull_stm = data;
        this.itemIpstpnotnull_stm = new MatTableDataSource<_ipstpnotnull_stm>(data);
        this.ngAfterViewInit_ipstpNotnull_stm();

      }
    } catch (error: any) {
      console.log();
    }

  }
  async ipstpall() {
    // this.ipofccall = +this.all_notnullcase + +this.ipofcaccnull_count    ipofcaccnull_sum ipofcaccnotnullstm_sum debit_notnull;
    this.ipstpsumdebit = +this.debit_notnull + +this.ipstpaccnull_sum + +this.ipstpaccnotnullstm_sum;
    //console.log('total',this.ipofcsumdebit);

  }

  async tab_ipstpnull(info) {
    this.itemIpstpNull = [];
    this.itemsExeclIpstpNull = [];
    try {
      let rs: any = await this.ipstpService.getipstpnull(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemsExeclIpstpNull = data;
        this.itemIpstpNull = new MatTableDataSource<_ipstpnull>(data);
        this.ngAfterViewInit_ipstpnull();

      }
    } catch (error: any) {
      console.log();
    }

  }

  async tab_ipstpnotnull(info) {
    this.itemIpstpnotNull = [];
    this.itemsExeclIpstpnotNull = [];
    try {
      let rs: any = await this.ipstpService.getipstpnotnull(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab2 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemsExeclIpstpnotNull = data;
        this.itemIpstpnotNull = new MatTableDataSource<_ipstpnotnull>(data);
        this.ngAfterViewInit_ipstpnotnull();

      }
    } catch (error: any) {
      console.log();
    }
  }

  async tab_ipstpaccbydate(info) {
    this.itemIpstpaccbydate = [];
    this.itemsExeclIpstpaccbydate = [];
    try {
      let rs: any = await this.ipstpService.getipstpaccbydate(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab3 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemsExeclIpstpaccbydate = data;
        this.itemIpstpaccbydate = new MatTableDataSource<_ipstpaccbydate>(data);
        this.ngAfterViewInit_ipstpaccbydate();

      }
    } catch (error: any) {
      console.log();
    }

  }

  async tab_ipstpbyerror(info) {
    this.itemerrorcode = [];
    this.itemerrorcodeExecl = [];
    try {
      let rs: any = await this.ipstpService.gettoperrorcode(info);
      let data: any = rs.results.rows;
      let xx: string = data
      this.countRowstab4 = xx.length
      // console.log(rs);
      if (rs.results.ok) {
        this.itemerrorcodeExecl = data;
        this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
        this.ngAfterViewInit_ipstperrorcode();

      }
    } catch (error: any) {
      console.log();
    }

  }
  exportAsXLSXNull(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpstpNull, 'ExporttoExcel');
  }
  exportAsXLSXNotNull_stm(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpstpNotNull_stm, 'ExporttoExcel');
  }
  exportAsXLSXnotNull(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpstpnotNull, 'ExporttoExcel');
  }
  exportAsXLSXAccbyDate(): void {
    this.excelService.exportAsExcelFile(this.itemsExeclIpstpaccbydate, 'ExporttoExcel');
  }
  exportAsXLSXErrorcode(): void {
    this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
  }
  //get debt_account
  onOptionsSelected() {
    this.isCheckbox = false;
    console.log('ACC', this.selecteditemaccount);

  }
  logCheckbox() {
    if (this.isCheckbox === true) {
      this.selecteditemaccount = '';
    }
    console.log(this.isCheckbox);
  }

  _setDataSource(indexNumber) {
    setTimeout(() => {
      switch (indexNumber) {
        case 0:
          !this.itemIpstpNull.paginator ? this.itemIpstpNull.paginator = this.paginator : null;
          break;
        case 1:
            !this.itemsExeclIpstpNotNull_stm.paginator ? this.itemsExeclIpstpNotNull_stm.paginator = this.paginator2 : null;          
            break;
        case 2:
          !this.itemIpstpnotNull.paginator ? this.itemIpstpnotNull.paginator = this.paginator3 : null;
          break;
        case 3:
          !this.itemIpstpaccbydate.paginator ? this.itemIpstpaccbydate.paginator = this.paginator4 : null;
          break;
        case 4:
          !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator5 : null;
      }
    });
  }

  circularprogress() {
    this.percent_null = Math.round(this.ipstpaccnull_count * 100 / this.ipstpcall)
    this.percent_rep = Math.round(this.ipstpaccnotnullstm_count * 100 / this.ipstpcall)
    this.percent_success = Math.round(this.all_notnullcase * 100 / this.ipstpcall)
    console.log('rep_percent', this.percent_null);

  }
}