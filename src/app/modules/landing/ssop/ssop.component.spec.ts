import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SsopComponent } from './ssop.component';

describe('SsopComponent', () => {
  let component: SsopComponent;
  let fixture: ComponentFixture<SsopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SsopComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SsopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
