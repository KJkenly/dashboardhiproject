import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';


import { Export16fRoutingModule } from './export16f-routing.module';
import { Export16fComponent } from './export16f.component';

import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    Export16fComponent
  ],
  providers:[ExcelService],
  imports: [
    Export16fRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts:() => import('echarts')
    })
  ]
})
export class Export16fModule { }
