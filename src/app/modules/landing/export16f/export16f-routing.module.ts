import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Export16fComponent } from './export16f.component';

const routes: Routes = [
  {
    path:'',component:Export16fComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Export16fRoutingModule { }
