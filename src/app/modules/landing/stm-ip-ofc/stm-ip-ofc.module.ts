import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmIpOfcRoutingModule } from './stm-ip-ofc-routing.module';
import { StmIpOfcComponent } from './stm-ip-ofc.component';
import { ExcelService } from 'app/services/excel.service';
@NgModule({
  declarations: [
    StmIpOfcComponent
  ],
  providers:[ExcelService],
  imports: [
    StmIpOfcRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmIpOfcModule { }
