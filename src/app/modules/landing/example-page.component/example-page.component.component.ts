import { Component } from '@angular/core';

@Component({
  selector: 'app-example-page.component',
  templateUrl: './example-page.component.component.html',
  styleUrls: ['./example-page.component.component.scss']
})
export class ExamplePageComponentComponent {

  constructor() {
    console.log('example page');
  }

}
