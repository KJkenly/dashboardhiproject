import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StmOpLgoRoutingModule } from './stm-op-lgo-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StmOpLgoRoutingModule
  ]
})
export class StmOpLgoModule { }
