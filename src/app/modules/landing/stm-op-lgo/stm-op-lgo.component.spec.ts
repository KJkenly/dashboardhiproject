import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmOpLgoComponent } from './stm-op-lgo.component';

describe('StmOpLgoComponent', () => {
  let component: StmOpLgoComponent;
  let fixture: ComponentFixture<StmOpLgoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmOpLgoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmOpLgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
