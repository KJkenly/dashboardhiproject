import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmIpBkkComponent } from './stm-ip-bkk.component';

describe('StmIpBkkComponent', () => {
  let component: StmIpBkkComponent;
  let fixture: ComponentFixture<StmIpBkkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmIpBkkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmIpBkkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
