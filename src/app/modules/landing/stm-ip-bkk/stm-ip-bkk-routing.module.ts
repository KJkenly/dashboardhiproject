import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmIpBkkComponent} from '../stm-ip-bkk/stm-ip-bkk.component';
const routes: Routes = [
  {
    path:'',component:StmIpBkkComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmIpBkkRoutingModule { }
