import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepIpLgoComponent } from './rep-ip-lgo.component'
const routes: Routes = [
  {
    path:'',component:RepIpLgoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepIpLgoRoutingModule { }
