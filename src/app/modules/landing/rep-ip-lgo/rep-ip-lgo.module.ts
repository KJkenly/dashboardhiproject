import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { RepIpLgoRoutingModule } from './rep-ip-lgo-routing.module';
import { RepIpLgoComponent } from './rep-ip-lgo.component';

import { ExcelService } from 'app/services/excel.service';
@NgModule({
  declarations: [
    RepIpLgoComponent
  ],
  providers:[ExcelService],
  imports: [
    RepIpLgoRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepIpLgoModule { }
