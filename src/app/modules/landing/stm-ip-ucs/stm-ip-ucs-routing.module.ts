import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StmIpUcsComponent} from '../stm-ip-ucs/stm-ip-ucs.component';
const routes: Routes = [
  {
    path:'',component:StmIpUcsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StmIpUcsRoutingModule { }
