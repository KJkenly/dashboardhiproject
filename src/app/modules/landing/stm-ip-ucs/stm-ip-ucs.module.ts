import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmIpUcsRoutingModule } from './stm-ip-ucs-routing.module';
import {StmIpUcsComponent } from './stm-ip-ucs.component'
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmIpUcsComponent
  ],
  providers:[ExcelService],
  imports: [
    StmIpUcsRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmIpUcsModule { }
