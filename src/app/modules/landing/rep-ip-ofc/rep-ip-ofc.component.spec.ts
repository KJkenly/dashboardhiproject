import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepIpOfcComponent } from './rep-ip-ofc.component';

describe('RepIpOfcComponent', () => {
  let component: RepIpOfcComponent;
  let fixture: ComponentFixture<RepIpOfcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepIpOfcComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepIpOfcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
