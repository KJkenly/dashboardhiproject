import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmIpStpRoutingModule } from './stm-ip-stp-routing.module';
import {StmIpStpComponent } from './stm-ip-stp.component'
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmIpStpComponent
  ],
  providers:[ExcelService],
  imports: [
    StmIpStpRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmIpStpModule { }
