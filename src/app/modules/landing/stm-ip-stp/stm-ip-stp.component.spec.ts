import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmIpStpComponent } from './stm-ip-stp.component';

describe('StmIpStpComponent', () => {
  let component: StmIpStpComponent;
  let fixture: ComponentFixture<StmIpStpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmIpStpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmIpStpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
