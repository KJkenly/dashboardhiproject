import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StmOpUcsComponent } from './stm-op-ucs.component';

describe('StmOpUcsComponent', () => {
  let component: StmOpUcsComponent;
  let fixture: ComponentFixture<StmOpUcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StmOpUcsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StmOpUcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
