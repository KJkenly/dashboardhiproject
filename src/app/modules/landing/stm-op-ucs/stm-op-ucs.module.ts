import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { StmOpUcsRoutingModule } from './stm-op-ucs-routing.module';
import { StmOpUcsComponent } from './stm-op-ucs.component'
import { ExcelService } from 'app/services/excel.service';

@NgModule({
  declarations: [
    StmOpUcsComponent
  ],
  providers:[ExcelService],
  imports: [
    StmOpUcsRoutingModule,
    MatPaginatorModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class StmOpUcsModule { }
