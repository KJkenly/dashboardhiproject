import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepIpUcsComponent } from './rep-ip-ucs.component';
const routes: Routes = [
  {
    path:'',component:RepIpUcsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepIpUcsRoutingModule { }
