import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepIpUcsComponent } from './rep-ip-ucs.component';

describe('RepIpUcsComponent', () => {
  let component: RepIpUcsComponent;
  let fixture: ComponentFixture<RepIpUcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepIpUcsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepIpUcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
