import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import {Component} from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxEchartsModule } from 'ngx-echarts';

import { RepIpUcsRoutingModule } from './rep-ip-ucs-routing.module';
import { RepIpUcsComponent } from './rep-ip-ucs.component';

import { ExcelService } from 'app/services/excel.service';
import { CircularProgressComponent } from '../../../components/circular-progress/circular-progress.component'

@NgModule({
  declarations: [
    RepIpUcsComponent
  ],
  providers:[ExcelService],
  imports: [
    RepIpUcsRoutingModule,
    MatPaginatorModule,
    SharedModule,
    CircularProgressComponent,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ]
})
export class RepIpUcsModule { }
