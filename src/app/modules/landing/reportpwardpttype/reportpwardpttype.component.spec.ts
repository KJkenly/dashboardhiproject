import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportpwardpttypeComponent } from './reportpwardpttype.component';

describe('ReportpwardpttypeComponent', () => {
  let component: ReportpwardpttypeComponent;
  let fixture: ComponentFixture<ReportpwardpttypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportpwardpttypeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportpwardpttypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
