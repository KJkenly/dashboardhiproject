import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepOpUcsComponent} from './rep-op-ucs.component'

const routes: Routes = [
  {
    path:'',component:RepOpUcsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepOpUcsRoutingModule { }
