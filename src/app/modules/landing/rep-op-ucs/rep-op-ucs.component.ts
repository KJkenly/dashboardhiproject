import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-op-ucs/rep-op-ucs.model'

import { OpucsService } from '../../../services/opucs.service'

export interface _opucsnull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}
export interface _opucsnotnull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}

export interface _opucsaccbydate {
  visit_date: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}

export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}

@Component({
  selector: 'app-rep-op-ucs',
  templateUrl: './rep-op-ucs.component.html',
  styleUrls: ['./rep-op-ucs.component.scss']
})
export class RepOpUcsComponent {

//count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;

//สร้างตัวแปร items,datestart,dateend
//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];

//opucsaccbydate
itemopucsaccbydate : any =[];
itemsExeclopucsaccbydate:any = [];

//opucsnotNull
itemopucsnotNull : any = [];
itemsExeclopucsnotNull:any = [];

//opucsNull
itemopucsNull : any = [];
itemsExeclopucsNull:any = [];

//accnull
opucsaccnull_count : any=0;
opucsaccnull_sum : any=0;

 //debt_account
 itemaccount:any = [];
 selecteditemaccount:any='';

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
opucscall : any =0;
opucssumdebit : any =0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;

opucsnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name', 'visit_date','visit_time','charge','paid','debt','repno','projcode','error_code','error_name','total_summary'];
opucsnotnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name','visit_date', 'visit_time','charge','paid','debt','repno','projcode','error_code','error_name',
'total_summary'];

opucsaccbydatedisplayedColumns: string[] = ['visit_date','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];


@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;

ngAfterViewInit_opucsnull() {
    this.itemopucsNull.paginator = this.paginator;
  }
  ngAfterViewInit_opucsnotnull() {
    this.itemopucsnotNull.paginator = this.paginator2;
  }
  ngAfterViewInit_opucsaccbydate() {
    this.itemopucsaccbydate.paginator = this.paginator3;
  }

  ngAfterViewInit_opucserrorcode() {
    this.itemerrorcode.paginator = this.paginator4;
  }

  constructor(
    private router: Router,
    private opucsService:OpucsService,
    private excelService : ExcelService
) {} 


ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getopofcacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.opucsService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}
async getopofcacc(){

  let lcCode:any = this.selecteditemaccount; //OFC
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
  
 //ผู้ป่วยส่งเบิกทั้งหมด OK
 await this.opCountofcAll(info)
 //ดำเนินการเสร็จ OK 
 await this.opucsaccnotnull(info);
 //รอดำเนินการ OK 
 await this.opucsaccnull(info);
 //tab 1 ok
 await this.tab_opucsnull(info);
 //tab 2 ok
 await this.tab_opucsnotnull(info);
 //tab 3 ok
 await this.tab_opucsaccbydate(info);
 //tab 4 
 await this.tab_opucsbyerror(info);
}

async opCountofcAll(info){ 
 try {
   let rs: any = await this.opucsService.getstmopCount(info);
   let data:any = rs.results;
  //  console.log('xx',rs);
   if (rs.results.ok){
     this.opucscall = rs.results.rows[0].all_op ;

   }
 } catch (error: any) {
     console.log(error);
 }
}

async opucsaccnull(info){
  try {
    let rs: any = await this.opucsService.getopucsaccnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.opucsaccnull_count = rs.results.rows[0].count_vn;
      this.opucsaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.opucsaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}
async opucsaccnotnull(info){
  try {
    let rs: any = await this.opucsService.getopucsaccnotnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}

async opucsall(){
      // this.opucscall = +this.all_notnullcase + +this.opucsaccnull_count;
      this.opucssumdebit = +this.debit_notnull + +this.opucsaccnull_sum;
}

async tab_opucsnull(info){
  this.itemopucsNull= [];
  this.itemsExeclopucsNull=[];
  try {
      let rs: any = await this.opucsService.getopucsnull(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok){
        this.itemsExeclopucsNull = data;
        this.itemopucsNull = new MatTableDataSource<_opucsnull>(data);
        this.ngAfterViewInit_opucsnull();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_opucsnotnull(info){
      this.itemopucsnotNull= [];
      this.itemsExeclopucsnotNull=[];
      try {
          let rs: any = await this.opucsService.getopucsnotnull(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopucsnotNull = data;
            this.itemopucsnotNull = new MatTableDataSource<_opucsnotnull>(data);
            this.ngAfterViewInit_opucsnotnull();

          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_opucsaccbydate(info){
      this.itemopucsaccbydate= [];
      this.itemsExeclopucsaccbydate=[];
      try {
          let rs: any = await this.opucsService.getopucsaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopucsaccbydate = data;
            this.itemopucsaccbydate = new MatTableDataSource<_opucsaccbydate>(data);
            this.ngAfterViewInit_opucsaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_opucsbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.opucsService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_opucserrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}
      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclopucsNull, 'ExporttoExcel');
        }

      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclopucsnotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclopucsaccbydate, 'ExporttoExcel');
    }
    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
    }
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }

    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemopucsNull.paginator ? this.itemopucsNull.paginator = this.paginator : null;
            break;
          case 1:
            !this.itemopucsnotNull.paginator ? this.itemopucsnotNull.paginator = this.paginator2 : null;
            break;
          case 2:
            !this.itemopucsaccbydate.paginator ? this.itemopucsaccbydate.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator4 : null;
        }
      });
    }
}