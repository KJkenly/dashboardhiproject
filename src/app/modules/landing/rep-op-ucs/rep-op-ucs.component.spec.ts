import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepOpUcsComponent } from './rep-op-ucs.component';

describe('RepOpUcsComponent', () => {
  let component: RepOpUcsComponent;
  let fixture: ComponentFixture<RepOpUcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepOpUcsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepOpUcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
