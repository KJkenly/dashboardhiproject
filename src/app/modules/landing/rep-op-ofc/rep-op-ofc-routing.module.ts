import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepOpOfcComponent } from './rep-op-ofc.component'
const routes: Routes = [
  {
    path:'',component:RepOpOfcComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepOpOfcRoutingModule { }
