import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepOpStpComponent } from './rep-op-stp.component';

describe('RepOpStpComponent', () => {
  let component: RepOpStpComponent;
  let fixture: ComponentFixture<RepOpStpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepOpStpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RepOpStpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
