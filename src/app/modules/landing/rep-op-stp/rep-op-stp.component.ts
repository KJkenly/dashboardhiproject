import { Component,ViewEncapsulation,ViewChild } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ApexOptions } from 'ng-apexcharts';
import { DateTime } from 'luxon';
import { constant } from 'lodash';
import * as moment from 'moment-timezone'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { formatNumber } from '@angular/common'
import { AlertService } from '../../../services/alert.service';
import { EChartsOption } from 'echarts';
import { ExcelService } from 'app/services/excel.service';
import { ChartType } from '../../landing/rep-op-stp/rep-op-stp.model'

import { OpstpService } from '../../../services/opstp.service'

export interface _opstpnull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}
export interface _opstpnotnull {
  hn : any;
  vn: any;
  cid: any;
  fullname: any;
  acc_name: any;
  visit_date: any;
  visit_time: any;
  charge: any;
  paid: any;
  debt: any;
  repno: any;
  error_code: any;
  error_name: any;
  total_summary: any;
  projcode: any;
}

export interface _opstpaccbydate {
  visit_date: any;
  allcase: any;
  debit: any;
  nullcase: any;
  nulldebit: any;
  notnullcase: any;
  notnulldebit: any;
  recieve: any;
  diff: any;
}

export interface _errorcode{
  error_code:any;
  error_name:any;
  counterrorcode:any;
}

@Component({
  selector: 'app-rep-op-stp',
  templateUrl: './rep-op-stp.component.html',
  styleUrls: ['./rep-op-stp.component.scss']
})
export class RepOpStpComponent {

//count datatable
countRowstab1 :any;
countRowstab2 :any;
countRowstab3 :any;
countRowstab4 :any;

//สร้างตัวแปร items,datestart,dateend
//itemerrorcode
itemerrorcode:any=[];
itemerrorcodeExecl:any=[];

//opstpaccbydate
itemopstpaccbydate : any =[];
itemsExeclopstpaccbydate:any = [];

//opstpnotNull
itemopstpnotNull : any = [];
itemsExeclopstpnotNull:any = [];

//opstpNull
itemopstpNull : any = [];
itemsExeclopstpNull:any = [];

//accnull
opstpaccnull_count : any=0;
opstpaccnull_sum : any=0;

 //debt_account
 itemaccount:any = [];
 selecteditemaccount:any='';

//accnotnull
all_notnullcase: any=0;
debit_notnull: any=0;
recieve: any=0;
sum_diff: any=0;
diffgain: any=0;
diffloss: any=0;
opstpcall : any =0;
opstpsumdebit : any =0;

startdate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
enddate: any = moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
isCheckbox : boolean = false;

// ข้อมูลฟิลด์ไหนต้องการให้แสดงให้ใส่ที่  displayedColumns
opstpnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name', 'visit_date','visit_time','charge','paid','debt','repno','projcode','error_code','error_name','total_summary'];
opstpnotnulldisplayedColumns: string[] = ['vn','hn','cid','fullname',
'acc_name','visit_date', 'visit_time','charge','paid','debt','repno','projcode','error_code','error_name',
'total_summary'];

opstpaccbydatedisplayedColumns: string[] = ['visit_date','allcase','debit','nullcase',
'nulldebit', 'notnullcase', 'notnulldebit','recieve','diff'];

errorcodedisplayedColumns: string[] = ['error_code','error_name','counterror'];

@ViewChild('paginator') paginator: MatPaginator;
@ViewChild('paginator2') paginator2: MatPaginator;
@ViewChild('paginator3') paginator3: MatPaginator;
@ViewChild('paginator4') paginator4: MatPaginator;

ngAfterViewInit_opstpnull() {
    this.itemopstpNull.paginator = this.paginator;
  }
  ngAfterViewInit_opstpnotnull() {
    this.itemopstpnotNull.paginator = this.paginator2;
  }
  ngAfterViewInit_opstpaccbydate() {
    this.itemopstpaccbydate.paginator = this.paginator3;
  }
  ngAfterViewInit_opstperrorcode() {
    this.itemerrorcode.paginator = this.paginator4;
  }
  constructor(
    private router: Router,
    private opstpService:OpstpService,
    private excelService : ExcelService
) {} 


ngOnInit(): void {
  this.isCheckbox  = true;
  this.getLoadData()
  //this.getopofcacc()
}

//get DATA TABLE
async getLoadData(){
    await this.getLookup_debtaccount();
}
//get DATA Table debt_account of db HICM
async getLookup_debtaccount(){
  let lcACC :any = await this.opstpService.getselect_debt_account_hi();
  console.log('lcACC',lcACC);
  
  this.itemaccount = lcACC;
}
async getopofcacc(){

  let lcCode:any = this.selecteditemaccount; //OFC
  let info: any = {
  "accType": lcCode,
  "startDate":moment(this.startdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
  "endDate":moment(this.enddate).tz('Asia/Bangkok').format('YYYY-MM-DD')
  }
  console.log('info:',info);
 //ผู้ป่วยส่งเบิกทั้งหมด OK
 await this.opCountofcAll(info)
 //ดำเนินการเสร็จ OK 
 await this.opstpaccnotnull(info);
 //รอดำเนินการ OK 
 await this.opstpaccnull(info);
 //tab 1 ok
 await this.tab_opstpnull(info);
 //tab 2 ok
 await this.tab_opstpnotnull(info);
 //tab 3 ok
 await this.tab_opstpaccbydate(info);
 //tab 4 
 await this.tab_opstpbyerror(info);
}

async opCountofcAll(info){ 
 try {
   let rs: any = await this.opstpService.getstmopCount(info);
   let data:any = rs.results;
  //  console.log('xx',rs);
   if (rs.results.ok){
     this.opstpcall = rs.results.rows[0].all_op ;

   }
 } catch (error: any) {
     console.log(error);
 }
}
async opstpaccnull(info){
  try {
    let rs: any = await this.opstpService.getopstpaccnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.opstpaccnull_count = rs.results.rows[0].count_vn;
      this.opstpaccnull_sum = rs.results.rows[0].sum_debt;
      console.log(this.opstpaccnull_count);

    }
  } catch (error: any) {
      console.log();

  }

}
async opstpaccnotnull(info){
  try {
    let rs: any = await this.opstpService.getopstpaccnotnull(info);
    // console.log(rs);
    if (rs.results.ok){
      // this.itemsExecl = rs;
      this.all_notnullcase = rs.results.rows[0].all_notnullcase;
      this.debit_notnull = rs.results.rows[0].debit_notnull;
      this.recieve = rs.results.rows[0].recieve;
      this.sum_diff = rs.results.rows[0].sum_diff;
      this.diffgain = rs.results.rows[0].diffgain;
      this.diffloss = rs.results.rows[0].diffloss;

    }
  } catch (error: any) {
      console.log();
  }

}

async opstpall(){
     // this.opstpcall = +this.all_notnullcase + +this.opofcaccnull_count;
      this.opstpsumdebit = +this.debit_notnull + +this.opstpaccnull_sum;
}

async tab_opstpnull(info){
  this.itemopstpNull= [];
  this.itemsExeclopstpNull=[];
  try {
      let rs: any = await this.opstpService.getopstpnull(info);
      let data:any = rs.results.rows;
      let xx:string = data
      this.countRowstab1 = xx.length
      // console.log(rs);
      if (rs.results.ok){
        this.itemsExeclopstpNull = data;
        this.itemopstpNull = new MatTableDataSource<_opstpnull>(data);
        this.ngAfterViewInit_opstpnull();

      }
    } catch (error: any) {
        console.log();
    }

  }

  async tab_opstpnotnull(info){
      this.itemopstpnotNull= [];
      this.itemsExeclopstpnotNull=[];
      try {
          let rs: any = await this.opstpService.getopstpnotnull(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab2 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopstpnotNull = data;
            this.itemopstpnotNull = new MatTableDataSource<_opstpnotnull>(data);
            this.ngAfterViewInit_opstpnotnull();

          }
        } catch (error: any) {
            console.log();
        }
  }

  async tab_opstpaccbydate(info){
      this.itemopstpaccbydate= [];
      this.itemsExeclopstpaccbydate=[];
      try {
          let rs: any = await this.opstpService.getopstpaccbydate(info);
          let data:any = rs.results.rows;
          let xx:string = data
          this.countRowstab3 = xx.length
          // console.log(rs);
          if (rs.results.ok){
            this.itemsExeclopstpaccbydate = data;
            this.itemopstpaccbydate = new MatTableDataSource<_opstpaccbydate>(data);
            this.ngAfterViewInit_opstpaccbydate();

          }
        } catch (error: any) {
            console.log();
        }

  }
  async tab_opstpbyerror(info){
    this.itemerrorcode= [];
    this.itemerrorcodeExecl=[];
    try {
        let rs: any = await this.opstpService.gettoperrorcode(info);
        let data:any = rs.results.rows;
        let xx:string = data
        this.countRowstab4 = xx.length
        // console.log(rs);
        if (rs.results.ok){
          this.itemerrorcodeExecl = data;
          this.itemerrorcode = new MatTableDataSource<_errorcode>(data);
          this.ngAfterViewInit_opstperrorcode();

        }
      } catch (error: any) {
          console.log();
      }

}
      exportAsXLSXNull():void {
          this.excelService.exportAsExcelFile(this.itemsExeclopstpNull, 'ExporttoExcel');
        }

      exportAsXLSXnotNull():void {
       this.excelService.exportAsExcelFile(this.itemsExeclopstpnotNull, 'ExporttoExcel');
     }

     exportAsXLSXAccbyDate():void {
      this.excelService.exportAsExcelFile(this.itemsExeclopstpaccbydate, 'ExporttoExcel');
    }
    exportAsXLSXErrorcode():void {
      this.excelService.exportAsExcelFile(this.itemerrorcode, 'ExporttoExcel');
    }
    //get debt_account
    onOptionsSelected(){
      this.isCheckbox  = false;
      console.log('ACC',this.selecteditemaccount);
      
    }
    logCheckbox() {
      if(this.isCheckbox === true) {
        this.selecteditemaccount = '';
      }
      console.log(this.isCheckbox);
    }

    _setDataSource(indexNumber) {
      setTimeout(() => {
        switch (indexNumber) {
          case 0:
            !this.itemopstpNull.paginator ? this.itemopstpNull.paginator = this.paginator : null;
            break;
          case 1:
            !this.itemopstpnotNull.paginator ? this.itemopstpnotNull.paginator = this.paginator2 : null;
            break;
          case 2:
            !this.itemopstpaccbydate.paginator ? this.itemopstpaccbydate.paginator = this.paginator3 : null;
            break;
          case 3:
            !this.itemerrorcode.paginator ? this.itemerrorcode.paginator = this.paginator4 : null;
        }
      });
    }

}