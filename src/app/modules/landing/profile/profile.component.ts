import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProfileService } from '../../../services/profile.service';
import * as moment from 'moment-timezone';
import { AlertService} from '../../../services/alert.service';


interface Food {
    value: string;
    viewValue: string;
  }

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
    foods: Food[] = [
        {value: 'steak-0', viewValue: 'Steak'},
        {value: 'pizza-1', viewValue: 'Pizza'},
        {value: 'tacos-2', viewValue: 'Tacos'},
      ];

      fullname: any;
      cid: any;
      code: any;
      accesstype: any;
      is_active: any;
      password: any;
      constructor(
        private router: Router,
        private profileService:ProfileService,
        private alertService : AlertService
    ) {}

ngOnInit(): void{
        this.getInfo();
        // this.getdprtm();
      }
async getInfo(){
 //ส่งค่าตัวแปร datestart,dateend เป็น Object

 let info: any = {"cid":sessionStorage.getItem('cid')}
 console.log(info);

 try {
   let rs: any = await this.profileService.select_raw_cid(info);
   let data:any = rs[0];
   console.log(rs);
   if (rs.length > 0){
        this.fullname = data.fullname
        this.cid = data.cid
        this.accesstype = data.accesstype
        this.is_active = data.is_active
        this.password = data.psswrd

   }
 } catch (error: any) {
     console.log();

 }
}

// async getdprtm(){
//     try {
//         let rs: any = await this.profileService.select_dprtm();
//         this.data_dprtm = rs[0];
//         this.data_dprtms = this.data_dprtm;
//         console.log(rs);
//         if (rs.length > 0){
//              this.name_dprtm = this.data_dprtm.name
//              this.code_dprtm = this.data_dprtm.code

//         }
//       } catch (error: any) {
//           console.log();

//       }
// }

}

