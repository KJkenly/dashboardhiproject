import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IpDashboardComponent } from './ip-dashboard.component';

describe('IpDashboardComponent', () => {
  let component: IpDashboardComponent;
  let fixture: ComponentFixture<IpDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IpDashboardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IpDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
