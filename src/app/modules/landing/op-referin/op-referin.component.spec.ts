import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpReferinComponent } from './op-referin.component';

describe('OpReferinComponent', () => {
  let component: OpReferinComponent;
  let fixture: ComponentFixture<OpReferinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpReferinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OpReferinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
