import { Route } from '@angular/router';
import { DashboardComponent } from 'app/modules/landing/dashboard/dashboard.component';

export const DashboardRoutes: Route[] = [
    {
        path     : '',
        component: DashboardComponent
    }
];
