import { Component, OnInit,HostListener, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
//import LoginService
import { LoginService } from 'app/services/login.service';
//import sweetalert2
import { AlertService } from 'app/services/alert.service';
import { AuthUtils } from 'app/core/auth/auth.utils'

@Component({
    selector     : 'auth-sign-in',
    templateUrl  : './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignInComponent implements OnInit
{
    @ViewChild('signInNgForm') signInNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signInForm: UntypedFormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private _router: Router,
        //เพิ่มตัวแปรรับค่า Class LoginService จาก login.service.ts
        private _loginService: LoginService,
        //เพิ่มตัวแปรรับค่า Class AlertService จาก alert.service.ts
        private _alertService: AlertService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signInForm = this._formBuilder.group({
            username  : ['', Validators.required],
            password  : ['', Validators.required]
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    async signIn()
    {
        // Return if the form is invalid
        if ( this.signInForm.invalid )
        {
            return;
        }

        // Disable the form
        // this.signInForm.disable();

        // Hide the alert
        // this.showAlert = false;

        // Sign in
        let info: any = this.signInForm.value;
        console.log(info);
        
        try {
            //ตัวแปร rs เพื่อเรียกใช้ class _loginService และฟังก์ชั่น login ส่งค่า token กลับมา
            let rs: any = await this._loginService.login(info.username,info.password);
            console.log(rs);
            //sessionStorage.setItem รับค่า rs.token
            sessionStorage.setItem('accessToken',rs.token);
            //
            const accessToken: any = AuthUtils._decodeToken(rs.token);
            // sessionStorage.setItem('codewd',accessToken.code);
            sessionStorage.setItem('fullname',accessToken.fullname);
            sessionStorage.setItem('is_active',accessToken.is_active);
            sessionStorage.setItem('cid',accessToken.cid)

            // this._router.navigate(['/opdashboard']);
            this._router.navigate(['/rep-op-ofc']);

        } catch (error: any) {
            console.log(error);
            //messagebox 1
            //this._alertService.error(error.error.message);
            //messagebox fix
            this._alertService.error('Username หรือ Password ไม่ถูกต้อง....!!!!');
        }


    }
}
