import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-circular-progress',
  standalone:true,
  templateUrl: './circular-progress.component.html',
  styleUrls: ['./circular-progress.component.scss']
})
export class CircularProgressComponent {
    @Input() percent = 0;
    @Input() color = '#fff';
    @Input() title = '';
    @Input() bg='#00171C';
 }


