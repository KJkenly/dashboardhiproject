// patient-image.pipe.ts
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'patientImage'
})
export class PatientImagePipe implements PipeTransform {
  transform(age: string, sex: string): string {
    const ageYear = Number(age);
    
    const imageType = sex === '1' ? 
      (ageYear < 0 ? 'newbornboy' :
       ageYear < 15 ? 'boy' :
       ageYear < 40 ? 'teenman' :
       ageYear < 60 ? 'uncleman' : 'oldman') :
      (ageYear < 0 ? 'newborngirl' :
       ageYear < 15 ? 'girl' :
       ageYear < 40 ? 'teengirl' :
       ageYear < 60 ? 'unclegirl' : 'oldgirl');

    return `assets/images/patientimage/${imageType}.png`;
  }
}