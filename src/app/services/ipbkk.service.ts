import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class IpbkkService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-ip-bkk/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-ip-bkk/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getipbkknull(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkknull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      
      getipbkkaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkkaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipbkkaccnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkkaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipbkknotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkknotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipbkkaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkkaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipbkk(info){
        const _url = `${this.apiUrl}/hicm-ip-bkk/stm_ip_bkk`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipbkk_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-bkk/stm_ip_bkk_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipbkk_detail(info){
        const _url = `${this.apiUrl}/hicm-stm-ip-bkk/stm_ip_bkk_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipbkk_sum(info){
        const _url = `${this.apiUrl}/hicm-ip-bkk/stm_ip_bkk_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipbkk_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-bkk/stm_ip_bkk_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipCount(info){
        const _url = `${this.apiUrl}/hicm-ip-bkk/stm_countan`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-ip-bkk/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }   
      getipbkknotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkknotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipbkkaccnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-bkk/ipbkkaccnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }             
  }
