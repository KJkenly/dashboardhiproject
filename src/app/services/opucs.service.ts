import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class OpucsService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
    
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getopucsnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ucs/opucsnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopucsaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ucs/opucsaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopucsaccnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ucs/opucsaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopucsnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-ucs/opucsnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopucsaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-op-ucs/opucsaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopucs(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/stm_op_ucs`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopucs_detail(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/stm_op_ucs_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopucs_sum(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/stm_op_ucs_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopucs_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/stm_op_ucs_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopCount(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/stm_countvn`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      getstmopucs_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-ucs/stm_op_ucs_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }      
  }
