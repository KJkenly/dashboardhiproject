import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class IpucsService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-ip-ucs/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-ip-ucs/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getipucsnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipucsaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipucsaccnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipucsnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipucsaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipucs(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/stm_ip_ucs`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipucs_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/stm_ip_ucs_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipucs_detail(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/stm_ip_ucs_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipucs_sum(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/stm_ip_ucs_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipucs_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/stm_ip_ucs_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipCount(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/stm_countan`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }   
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-ip-ucs/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipucsnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipucsaccnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-ucs/ipucsaccnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }    

  }
