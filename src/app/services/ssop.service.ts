import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SsopService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
    async select_idpm() {
      const _url = `${this.apiUrl}/ssop/select_idpm`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
    }
    async select_cln() {
      const _url = `${this.apiUrl}/ssop/select_cln`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
    }
    async select_yearbudget() {
    const _url = `${this.apiUrl}/ssop/select_yearbudget`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
    }

    getssop_all_status(info) {
      const _url = `${this.apiUrl}/ssop/ssop_all_statusflg`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    getssop_statusflgN(info) {
      const _url = `${this.apiUrl}/ssop/ssop_statusflgN`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getssop_statusflgA(info) {
      const _url = `${this.apiUrl}/ssop/ssop_statusflgA`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getssop_statusflgE(info) {
      const _url = `${this.apiUrl}/ssop/ssop_statusflgE`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getssop_statusflgD(info) {
      const _url = `${this.apiUrl}/ssop/ssop_statusflgD`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }



  }
