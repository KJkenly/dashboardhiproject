import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IoprtService {

    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }

      async select_cln() {
        const _url = `${this.apiUrl}/dashboard/select_cln`;
        return this.httpClient.get(_url, this.httpOptions).toPromise();
     }
     async select_yearbudget() {
      const _url = `${this.apiUrl}/dashboard/select_yearbudget`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
    }

    async getReferoutSummary(info: any) {
        const _url = `${this.apiUrl}/op-refer/getOPReferoutsummary`;       
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    async getReferinSummary(info: any) {
      const _url = `${this.apiUrl}/op-refer/getOPReferinsummary`;       
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }
  async getnamecln(info: any) {
    const _url = `${this.apiUrl}/op-refer/getnamecln`;       
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }
  getDx20Referout(info) {
    const _url = `${this.apiUrl}/op-refer/getDx20Referout`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  } 

  getDx20Referin(info) {
    const _url = `${this.apiUrl}/op-refer/getDx20Referin`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  } 

  getCLN20Referout(info) {
    const _url = `${this.apiUrl}/op-refer/getCLN20Referout`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }   
  getCLN20Referin(info) {
    const _url = `${this.apiUrl}/op-refer/getCLN20Referin`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }   
  getRfrcs20Referout(info) {
    const _url = `${this.apiUrl}/op-refer/getRfrcs20Referout`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }   
  getRfrcs20Referin(info) {
    const _url = `${this.apiUrl}/op-refer/getRfrcs20Referin`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }  
  getHcode20Referout(info) {
    const _url = `${this.apiUrl}/op-refer/getHcode20Referout`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }  
  getHcode20Referin(info) {
    const _url = `${this.apiUrl}/op-refer/getHcode20Referin`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
  }    
}

