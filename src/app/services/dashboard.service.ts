import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }

      async select_cln() {
        const _url = `${this.apiUrl}/dashboard/select_cln`;
        return this.httpClient.get(_url, this.httpOptions).toPromise();
     }
     async select_yearbudget() {
      const _url = `${this.apiUrl}/dashboard/select_yearbudget`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
    }
      async getService_opd_all(info: any) {
          console.log(info);         
          const _url = `${this.apiUrl}/dashboard/service_opd_all`;
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
        }
      async getService_opd_null(info: any) {
          console.log(info);
          const _url = `${this.apiUrl}/dashboard/service_opd_null`;
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      async getService_opd_home(info: any) {
          console.log(info);
          const _url = `${this.apiUrl}/dashboard/service_opd_home`;
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      async getService_opd_dead(info: any) {
        const _url = `${this.apiUrl}/dashboard/service_opd_dead`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_opd_refer(info: any) {
      const _url = `${this.apiUrl}/dashboard/service_opd_refer`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_opd_refer_in(info: any) {
        const _url = `${this.apiUrl}/dashboard/service_opd_refer_in`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getService_opd_admit(info: any) {
      const _url = `${this.apiUrl}/dashboard/service_opd_admit`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getServiceTotal_ipd(info: any) {
          const _url = `${this.apiUrl}/dashboard/service_opd_admit`;
          console.log(_url);
          
          return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      async getOpSummary(info: any) {
        const _url = `${this.apiUrl}/dashboard/getOPsummary`;       
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    async getreload_referout(){
      window.open(`${window.location.origin}/op-rfro`,"_blank");
    }
    
    async getreload_referin(){
      window.open(`${window.location.origin}/op-rfri`,"_blank");
    }
  }

