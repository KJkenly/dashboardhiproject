import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  accessToken: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    //สร้างตัวแปร accessToken รับค่า accessToken
    this.accessToken = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({

        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' +this.accessToken
      })
    };
  }

    async select() {
      const _url = `${this.apiUrl}/users/select`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
   }

    async insert(info:object) {
      const _url = `${this.apiUrl}/users/insert`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    async update(id:any,info:object,) {
      const _url = `${this.apiUrl}/users/update/${id}`;
      return this.httpClient.put(_url,info,this.httpOptions).toPromise();
    }

    async delete(id:any) {
      const _url = `${this.apiUrl}/users/delete/${id}`;
      return this.httpClient.delete(_url,this.httpOptions).toPromise();
    }

    async select_raw_code(info: any) {
      const _url = `${this.apiUrl}/users/select_raw_code`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    async select_raw_cid(info: any) {
      const _url = `${this.apiUrl}/users/select_raw_cid`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    async select_dprtm() {
        const _url = `${this.apiUrl}/users/select_dprtm`;
        return this.httpClient.get(_url, this.httpOptions).toPromise();
     }


}
