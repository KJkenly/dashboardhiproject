import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class BookclaimService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }

    getbookclaim_all_statusflg(info) {
      const _url = `${this.apiUrl}/book-claim/bookclaim_all_statusflg`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    getselete_raw_bookclaim(info) {
      const _url = `${this.apiUrl}/book-claim/selete_raw_bookclaim`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

  }
