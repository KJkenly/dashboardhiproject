import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class IpofcService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-ip-ofc/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-ip-ofc/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getipofcnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipofcaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipofcaccnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipofcnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getipofcaccnotnull_stm(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcaccnotnull_stm`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }     
      getipofcnotnull(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getipofcaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-ip-ofc/ipofcaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipofc(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/stm_ip_ofc`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipofc_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/stm_ip_ofc_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmipofc_detail(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/stm_ip_ofc_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipofc_sum(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/stm_ip_ofc_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipofc_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/stm_ip_ofc_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmipCount(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/stm_countan`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-ip-ofc/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }      
  }
