import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class Export16fService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
    getselect_lfunds(){
      const _url = `${this.apiUrl}/16f/select_lfund`;
      return this.httpClient.post(_url,this.httpOptions).toPromise();
    }
    
    getins(info){
        const _url = `${this.apiUrl}/16f/getins`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getpat(info){
      const _url = `${this.apiUrl}/16f/getpat`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getopd(info){
      const _url = `${this.apiUrl}/16f/getopd`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getorf(info){
    const _url = `${this.apiUrl}/16f/getorf`;
    return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    } 
    getodx(info){
      const _url = `${this.apiUrl}/16f/getodx`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }  
    getoop(info){
      const _url = `${this.apiUrl}/16f/getoop`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }  
    getipd(info){
      const _url = `${this.apiUrl}/16f/getipd`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    } 
    getirf(info){
      const _url = `${this.apiUrl}/16f/getirf`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    }   
    getidx(info){
      const _url = `${this.apiUrl}/16f/getidx`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    }   
    getiop(info){
      const _url = `${this.apiUrl}/16f/getiop`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    }  
    getcht(info){
      const _url = `${this.apiUrl}/16f/getcht`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    }  
    getcha(info){
      const _url = `${this.apiUrl}/16f/getcha`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    }
    getaer(info){
      const _url = `${this.apiUrl}/16f/getaer`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();      
    } 
                 
  }
