import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class OpstpService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
      getselect_debt_account(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getselect_debt_account_hi(){
        const _url = `${this.apiUrl}/hicm-op-bkk/select_debt_account_hi`;
        return this.httpClient.post(_url,this.httpOptions).toPromise();
      }
      getopstpnull(info) {
        const _url = `${this.apiUrl}/hicm-op-stp/opstpnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopstpaccnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-stp/opstpaccnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopstpaccnull(info) {
        const _url = `${this.apiUrl}/hicm-op-stp/opstpaccnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopstpnotnull(info) {
        const _url = `${this.apiUrl}/hicm-op-stp/opstpnotnull`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getopstpaccbydate(info) {
        const _url = `${this.apiUrl}/hicm-op-stp/opstpaccbydate`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopstp(info){
        const _url = `${this.apiUrl}/hicm-op-stp/stm_op_stp`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopstp_detail(info){
        const _url = `${this.apiUrl}/hicm-op-stp/stm_op_stp_detail`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }

      getstmopstp_sum(info){
        const _url = `${this.apiUrl}/hicm-op-stp/stm_op_stp_sum`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      getstmopCount(info){
        const _url = `${this.apiUrl}/hicm-op-stp/stm_countvn`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }
      gettoperrorcode(info){
        const _url = `${this.apiUrl}/hicm-op-stp/toperrorcode`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      getstmopstp_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-stp/stm_op_stp_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      } 
      getstmopstp_sum_stmno(info){
        const _url = `${this.apiUrl}/hicm-op-stp/stm_op_stp_sum_stmno`;
        return this.httpClient.post(_url,info,this.httpOptions).toPromise();
      }         
  }
