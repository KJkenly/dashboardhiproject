import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AipnService {
    accessToken: any;
    httpOptions: any;

    constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
      //สร้างตัวแปร accessToken รับค่า accessToken
      this.accessToken = sessionStorage.getItem('accessToken');
      this.httpOptions = {
        headers: new HttpHeaders({

          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +this.accessToken
        })
      };
    }
    async select_idpm() {
      const _url = `${this.apiUrl}/ip-dashboard/select_idpm`;
      return this.httpClient.get(_url, this.httpOptions).toPromise();
    }
    async select_yearbudget() {
    const _url = `${this.apiUrl}/ip-dashboard/select_yearbudget`;
    return this.httpClient.get(_url, this.httpOptions).toPromise();
    }

    getaipn_all_status(info) {
      const _url = `${this.apiUrl}/aipn/aipn_all_statusflg`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }

    getaipn_statusflgN(info) {
      const _url = `${this.apiUrl}/aipn/aipn_statusflgN`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getaipn_statusflgA(info) {
      const _url = `${this.apiUrl}/aipn/aipn_statusflgA`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getaipn_statusflgC(info) {
      const _url = `${this.apiUrl}/aipn/aipn_statusflgC`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }
    getaipn_statusflgD(info) {
      const _url = `${this.apiUrl}/aipn/aipn_statusflgD`;
      return this.httpClient.post(_url,info,this.httpOptions).toPromise();
    }



  }
